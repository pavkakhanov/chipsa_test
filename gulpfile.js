

// Gulp
const gulp = require('gulp');

// SASS, JADE
const sass = require('gulp-sass');
const pug = require('gulp-pug');
const autoprefixer = require('gulp-autoprefixer');

// ES6
const browserify = require('browserify');
const babelify = require('babelify');

// images
const pngquant = require('imagemin-pngquant');

// Server
const server = require('browser-sync');
// const reload = server.reload;

// Filesystem

// const watch = require('gulp-watch');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

// Minifires
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const minifyCss = require('gulp-minify-css');
const imagemin = require('gulp-imagemin');

// Misc
const rigger = require('gulp-rigger');
const uri = require('gulp-data-uri-stream');
const replace = require('gulp-replace');
const fileinclude = require('gulp-file-include');

// Server config
const config = {
  env: process.env.NODE_ENV || 'development',

  path: {
    build: {
      html: 'build',
      js: 'build/assets/js/',
      css: 'build/assets/css',
      images: 'build/assets/images',
      fonts: 'build/assets/fonts',
      svg: 'build/assets/svg/uri',
    },
    src: {
      jade: 'src/*.pug',
      ES: 'src/js/common.js',
      scss: 'src/scss/style.scss',
      images: 'src/images/**/*.*',
      fonts: 'src/fonts/**/*.*',
      svg: 'src/svg/*.*',

    },
    watch: {
      html: 'build/*.html',
      jade: 'src/**/*.pug',
      ES: 'src/js/common.js',
      scss: 'src/scss/**/*.scss',
      images: 'src/images/**/*.*',
      fonts: 'src/fonts/**/*.*',
      svg: 'src/svg/*.svg',

    },
    clean: 'build',
  },

  server: {
    port: 9000,
    host: 'localhost',
    logPrefix: 'NodeBuilder',
    tunnel: false,
    open: false,
    reloadDelay: 1000,
    reloadDebounce: 1000,
    server: {
      baseDir: 'build',
    },
    ui: {
      port: 3002,
    },
    ghostMode: {
      clicks: false,
      forms: false,
      scroll: false,
    },
    watchOptions: {
      ignoreInitial: true,
      ignore: '.gitkeep',
    },
  },

};

// Tasksbabel
gulp.task('jade::build', () => gulp.src(config.path.src.jade)
  .pipe(rigger())
  .pipe(pug({ pretty: true })
    .on('error', (e) => { console.log('OPPA ', e.message); }))
  .pipe(gulp.dest(config.path.build.html))
  .pipe(server.stream({ once: true })));

gulp.task('script::build', () => browserify({
  entries: [config.path.src.ES],
  debug: true,
  extensions: ['.js'],
  transform: babelify.configure({ presets: ['@babel/preset-env'] }),
}).bundle()
  .pipe(source('script.js'))
  .pipe(buffer())
  .pipe(sourcemaps.init({ loadMaps: false, debug: true }))
  .pipe(rigger())
  // .pipe(uglify())
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest(config.path.build.js))
  .pipe(server.stream()));

gulp.task('style::build', () => gulp.src(config.path.src.scss)
  .pipe(
    sass({
      includePaths: ['src/scss/partials'],
      imagePath: '/build/assets/images',
    }),
  )
  .pipe(fileinclude({ prefix: '@@', basepath: './' }))
  .pipe(autoprefixer('last 2 version', '> 1%', 'Explorer >= 8', { cascade: true }))
  .pipe(minifyCss({ compatibility: 'ie9' }))
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest(config.path.build.css))
  .pipe(server.stream()));


gulp.task('images::build', () => gulp.src(config.path.src.images)
  .pipe(imagemin({
    progressive: true,
    svgoPlugins: [
      { removeViewbox: false },
      { removeUselessStrokeAndFill: false },
      { removeEmptyAttrs: false },
    ],
    use: [pngquant()],
    interlaced: true,
  }))
  .pipe(gulp.dest(config.path.build.images))
  .pipe(server.stream()));

gulp.task('fonts::build', () => gulp.src(config.path.src.fonts)
  .pipe(gulp.dest(config.path.build.fonts)));

gulp.task('svg::build', () => (gulp.src(config.path.src.svg))
  .pipe(uri({ encoding: 'utf8' }))
  .pipe(replace(/;utf8/g, ''))
  .pipe(gulp.dest(config.path.build.svg)));

gulp.task('server', () => {
  server(config.server);
});


gulp.task('live', () => {
  gulp.watch([config.path.watch.jade], () => { gulp.start('jade::build'); });
  gulp.watch([config.path.watch.scss], () => { gulp.start('style::build'); });
  gulp.watch([config.path.watch.ES], () => { gulp.start('script::build'); });
  gulp.watch([config.path.watch.images], () => { gulp.start('images::build'); });
  gulp.watch([config.path.watch.fonts], () => { gulp.start('fonts::build'); });
  gulp.watch([config.path.watch.svg], () => { gulp.start('svg::build'); });
});

gulp.task('build::all', ['svg::build',
  'jade::build',
  'script::build',
  'fonts::build',
  'images::build',
  'style::build'], () => {
});


gulp.task('default', ['build::all', 'server', 'live']);
