
const images = document.images;
const imagesCount = images.length;
let loadedImages = 0;
const percent = document.querySelector('.page-preloader-top_progress-bar--progress');

for (let i = 0; i < images.length; i++) {
  const img_clone = new Image();
  img_clone.onload = imageLoaded;
  img_clone.onerror = imageLoaded;
  img_clone.src = images[i].src;
}

function imageLoaded() {
  loadedImages += 1;
  percent.style.width = `${((100 / imagesCount) * loadedImages).toFixed(0)}%`;

  if (loadedImages >= imagesCount) {
    setTimeout(() => {
      const preloader = document.querySelector('.page-preloader');
      const slide1 = document.querySelector('.slide-first');
      if (
        !preloader.classList.contains('page-preloader-done')
        && slide1.classList.contains('slide-first-not_loaded')
      ) {
        preloader.classList.add('page-preloader-done');
        slide1.classList.remove('slide-first-not_loaded');
      }
    }, 2000);
  }
}

const ui = {
  elements: {
    nav: document.querySelector('nav'),
    footer: document.querySelector('footer'),
    slide1: document.querySelector('.slide-first'),
    slide2: document.querySelector('.slide-second'),
    video: document.querySelector('.video-bg'),
  },

  switchTab(tab) {
    function hideElement(el) {
      el.classList.remove('tab-selected');
      el.classList.add('tab-hiding');
      setTimeout(() => {
        el.classList.remove('tab-hiding');
      }, 400);
    }
    function showElement(el) {
      el.classList.add('tab-selected');
    }

    const indicators = document.querySelectorAll('.tab-indicator');
    indicators.forEach((indicator) => {
      indicator.setAttribute('data-tab', tab);
    });

    const tabs = document.querySelectorAll('div.tab');

    tabs.forEach((slidetab) => {
      slidetab.classList.contains(`tab-${tab}`)
        ? showElement(slidetab)
        : hideElement(slidetab);
    });
  },

  bgVideoShow(e) {
    document.querySelector('.video-bg').setAttribute('data-mouse', e.type);
  },

  fullScreenVid(action) {
    const displayVideo = () => {
      Object.keys(this.elements).forEach((key) => {
        key === 'video'
          ? this.elements[key].classList.add('fullscreen')
          : this.elements[key].classList.add('hidden');
      });
    };

    const hideVideo = () => {
      Object.keys(this.elements).forEach((key) => {
        key === 'video'
          ? this.elements[key].classList.remove('fullscreen')
          : this.elements[key].classList.remove('hidden');
      });
    };

    switch (action) {
      case ('play'):
        displayVideo();
        break;
      case ('stop'):
        hideVideo();
        break;
      default:
        break;
    }
  },

  scrollToSecondSlide() {
    window.scrollTo({
      top: window.innerHeight,
      behavior: 'smooth',
    });
    // const slide2 = document.querySelector('.slide-second');
    // slide2.scrollIntoView({ block: 'end', behavior: 'smooth' });
  },
};


document.body.onclick = (e) => {
  const targetKey = Object.keys(e.target.dataset)[0];
  const targetValue = e.target.dataset[targetKey];
  switch (targetKey) {
    case ('tab'):
      ui.switchTab(targetValue);
      break;
    case ('play'):
      ui.fullScreenVid(targetValue);
      break;
    case ('slide'):
      ui.scrollToSecondSlide();
      break;
    default:
      break;
  }
};

document.body.onscroll = function slide2Pos() {
  this.slideOffset = ui.elements.slide2.offsetParent;
  this.navHeight = ui.elements.nav.offsetHeight;
  const plane = document.querySelector('.top-row_plane-pic');
  const offset = Math.abs(this.slideOffset.getBoundingClientRect().bottom);
  (offset < this.navHeight + 100)
    ? plane.classList.add('top-row_plane-pic--visible')
    : '';
};


const videoBgTrigger = document.querySelector('.middle-column_play');
videoBgTrigger.onmouseover = videoBgTrigger.onmouseout = ui.bgVideoShow;

let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);

window.onresize = () => {
  vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
};
